def say_hello():
	animals = ['cat', 'dog', 'hamster']
	food = [
		'ramen',
		'ramyun',
		'spaghetti', #trailing comma
		'udong',
	]
	colors = [
		'red', 
		'blue',
		'green',
		'yellow'
	]
	print("Hello, World!")


if __name__ == "__main__":
	say_hello()
